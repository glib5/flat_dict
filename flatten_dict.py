from typing import Any


class FlatteningError(Exception):
    pass


def _flatten_recursive(d:dict, seen:dict[str, Any]):
    for k, v in d.items():
        if isinstance(v, dict):
            _flatten_recursive(v, seen)
        elif k in seen:
            raise FlatteningError("Encountered the same key twice")
        else:
            seen[k] = v
     

def flatten_dict(d:dict) -> dict[str, Any]:
    seen = dict()
    _flatten_recursive(d, seen)
    return seen